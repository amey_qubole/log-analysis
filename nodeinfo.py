import subprocess


class ClassProperty(property):
    """To define properties which act as class methods"""
    def __get__(self, cls, owner):
        return self.fget.__get__(None, owner)()

class NodeInfo(object):
    @ClassProperty
    @classmethod
    def qubole_bash_lib_file(cls):
        """Returns the location of qubole-bash-lib.sh"""
        return "/usr/lib/hustler/bin/qubole-bash-lib.sh"

    @classmethod
    def get_info(cls, key):
        """Return value from nodeinfo"""
        proc = subprocess.Popen(["source {} && nodeinfo {}".format(
                                cls.qubole_bash_lib_file, key)], stdout=subprocess.PIPE,
                                shell=True, executable='/bin/bash')

        return proc.stdout.readline().decode('utf-8').rstrip("\n")