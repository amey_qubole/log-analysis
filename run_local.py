import argparse
import time

from build_lcs_map import build_lcs_map

def parse_args():
    parser = argparse.ArgumentParser()


    parser.add_argument('--input-path', type=str, default="data/log_analysis/",
                        help='Path to read files from.')
    parser.add_argument('--output-path', type=str, default=None,
                        help='Path to store output files.')
    parser.add_argument('--write-output', type=bool, default=False,
                        help='If to write the converted files.')
    parser.add_argument('--pattern-limit', type=int, default=8000,
                        help='Maximum number of patterns after which we stop processing a file.')            
    parser.add_argument('--file-limit', type=int, default=100,
                        help='Maximum number of files to process.')            

    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()

    timestamp = int(time.time())
    output_path = "{}_output".format(args.input_path.rstrip("/")) \
                    if args.output_path is None else args.input_path

    output_path = "{}/{}".format(output_path, timestamp)

    build_lcs_map(args.input_path, output_path,
                  args.write_output, args.file_limit, 
                  args.pattern_limit)