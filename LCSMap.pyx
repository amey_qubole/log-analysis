from io import StringIO
import re
import uuid

from LCSObject import LCSObject
from utils import is_number

class LCSMap(object):
    def __init__(self, write_output, pattern_limit):
        self.lcs_objects = []
        self.line_hashmap = {}
        self.clash_report = []
        self.write_output = write_output
        self.pattern_limit = pattern_limit
        self.file_count = 0

    def remove_duplicates(self):
        unique_objects = []

        if self.write_output:
            reverse_line_hashmap = {}
            new_line_hashmap = {}

            # Reverse the line hashmap
            for key, value in self.line_hashmap.items():
                if value not in reverse_line_hashmap:
                    reverse_line_hashmap[value] = []
                reverse_line_hashmap[value].append(key)

        # Remove duplicates
        for lcs_object in self.lcs_objects:
            match = [x for x in unique_objects if x.seq == lcs_object.seq]
            
            if len(match) == 0:
                unique_objects.append(lcs_object)
            else:
                match = match[0]
                match.merge(lcs_object)

                if self.write_output:
                    reverse_line_hashmap[match.id] += reverse_line_hashmap[lcs_object.id]
                    del(reverse_line_hashmap[lcs_object.id])

        if self.write_output:
            # Repopulate the hasmap
            for key, values in reverse_line_hashmap.items():
                for value in values:
                    new_line_hashmap[value] = key
            self.line_hashmap = new_line_hashmap

        self.lcs_objects = unique_objects
        

 
    def report_clash(self, input_seq, clashes):
        clash = {
            "input_seq": input_seq,
            "clashes": clashes
        }

        self.clash_report.append(clash)

    def process_file(self, file_name, buffer=None, remove_duplicates=True):
        print("Processing file: {}".format(file_name))
                
        input_file = buffer if buffer != None else open(file_name, "r")

        try:
            for line_id, line in enumerate(input_file):
                if len(self.lcs_objects) > self.pattern_limit:
                    print("Closing processing for file {} early, \
                        maximum pattern limit reached.".format(file_name))
                    return self
                else:
                    self.insert_line(line, (file_name, line_id))
            
            if buffer == None:
                input_file.close()
            
            if remove_duplicates:
                self.remove_duplicates()
        except UnicodeDecodeError:
            print("{} could not be decoded.".format(file_name))
        
        self.file_count += 1

        return self

    def convert_file(self, input_file_name, output_file_name=None,
                     write_to_buffer=False):
        if self.write_output:
            raise TypeError("""To call convert file the "write_output" 
                               property should be set to true.""")

        print("Converting file: {}".format(input_file_name))

        if write_to_buffer:
            output_file = StringIO()
        else:
            output_file = open(output_file_name, "w")

        write_lambda = lambda line_id: output_file.write(
                        "{}\n".format(self.line_hashmap[line_id]))
        
        line_number = 0

        while True:
            line_id = (input_file_name, line_number)

            if line_id in self.line_hashmap.keys():
                write_lambda(line_id)
                line_number += 1
            else:
                break

        if write_to_buffer:
            return output_file.getvalue()
        else:
            output_file.close()
            return None

    def insert_line(self, line, line_hash):
        if type(line) is not str:
            try:
                line = line.decode('utf-8')
            except:
                line = ''
        seq = re.split(';|,|\*|\s+|=|\.|#|_|-|/|:|\(|\)|\[|\]', line)
        seq = [x for x in seq if x != '']

        seq = self.process_line(seq)

        if len(seq) < 4 or len(seq) > 500:
            if self.write_output:
                self.line_hashmap[line_hash] = "NA"
            return

        lcs_object = self.get_match(seq, line_hash[0])
        
        if lcs_object == None:
            lcs_object = LCSObject(seq, str(uuid.uuid4()))
            self.lcs_objects.append(lcs_object)
        else:
            lcs_object.insert_seq(seq)
        
        if self.write_output:
            self.line_hashmap[line_hash] = lcs_object.id

    def process_line(self, seq):
        
        new_seq = []
        
        for token in seq:
            if is_number(token): # Remove numbers
                if len(new_seq) == 0 or new_seq[-1] != "*":
                    new_seq.append("*")
            elif len(token) > 25: # Remove tokens with length greater than 25 char
                continue
            else:
                new_seq.append(token)

        return new_seq

    def get_match(self, seq, input_file_name):
        original_count = len(self.lcs_objects)

        # Filter out based on the length of pattern and sequence length
        lcs_objects = list(filter(lambda x: x.get_length() > len(seq) / 2 \
                                and x.get_length() < len(seq) * 2, self.lcs_objects))

        filter_1_count = len(lcs_objects)

        lcs_objects = list(map(lambda x: (x.get_lcs_length(seq), x), lcs_objects))

        if len(lcs_objects) > 0:
            best_match = max(lcs_objects, key=lambda x: x[0])
        else:
            best_match = None

        # Get length excluding wild cards
        real_len = lambda seq: len([x for x in seq if x != "*"])

        # Filter out based on the match length and sequence length
        lcs_objects = list(filter(lambda x: x[0] >= real_len(seq) / 2 \
                                and x[0] >= real_len(x[1].seq) / 2, lcs_objects))

        filter_2_count = len(lcs_objects)

        try:
            sorted_by_length = sorted(lcs_objects, key=lambda x: x[0])

            clashes = [(x, y) for (x, y) in sorted_by_length if x == sorted_by_length[-1][0]]
            if len(clashes) > 1:
                self.report_clash(seq, clashes)

            return sorted_by_length[-1][1]

        except IndexError:
            self.print_insert_log(seq, input_file_name, best_match, original_count,
                             filter_1_count, filter_2_count)
            return None

    def get_size(self):
        return len(self.lcs_objects)

    def print_insert_log(self, seq, input_file_name,
                         best_match, original_count,
                         filter_1_count, filter_2_count):
        main_template = """
            -----------------------------------------------------
            Filename: {}
            File Number: {}
            Total existing patterns: {}
            Patterns after filter 1: {}
            Sequence length: {}
            Best match: {}
            Patterns after filter 2: {}
            Last added pattern: {}
            Creating new pattern, no existing pattern passes filters.
            Sequence: {}
            -----------------------------------------------------"""
        
        best_match_template = """
            Best match length: {}
            Best match seq: {}"""
        
        if best_match != None:
            best_match = best_match_template.format(best_match[0],
                                           " ".join(best_match[1].seq))

        last_added_pattern = None if len(self.lcs_objects) == 0 else self.lcs_objects[-1].seq

        print(main_template.format(input_file_name, self.file_count+1,original_count, filter_1_count,
                                   len(seq), best_match, filter_2_count, 
                                   last_added_pattern, seq))
    
    def merge(self, input_lcs_map):
        if self.write_output:
            self.line_hashmap = {**self.line_hashmap, **input_lcs_map.line_hashmap}

        self.lcs_objects += input_lcs_map.lcs_objects 
        self.clash_report += input_lcs_map.clash_report

        self.remove_duplicates()

        return self