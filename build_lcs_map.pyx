import glob
import os
import pickle
import time

from LCSMap import LCSMap


def build_lcs_map(input_path, output_path, 
                  write_output, file_limit,
                  pattern_limit):
    start_time = time.time()
    input_file_names = glob.glob('{}/*.err'.format(input_path))
    input_file_names = input_file_names[:file_limit]
    lcsmap = LCSMap(write_output, pattern_limit)

    # Process all the files
    print("Processing files")
    [lcsmap.process_file(x) for x in input_file_names]
    
    # Create the output dirs
    os.makedirs(os.path.join(output_path, "output"))

    # Store pickle
    print("Storing pickle.")
    pickel_file_path = os.path.join(output_path, "lcsmap.p")
    pickle.dump(lcsmap, open(pickel_file_path, "wb"))
    
    # Store all the patterns
    print("Storing patterns.")
    pattern_file_path = os.path.join(output_path, "lcs_patterns.txt")
    with open(pattern_file_path, "w") as f:
        [f.write("{}\n".format(str(x))) for x in lcsmap.lcs_objects]

    if write_output:
        # Write the processed files
        output_file_names = [os.path.join(output_path, "output", y) for (x, y) \
                            in [os.path.split(x) for x in input_file_names]]

        [lcsmap.convert_file(x, y) for (x, y) in zip(input_file_names, output_file_names)]
        
    end_time = time.time()
    print("Process took: {} secs".format(end_time - start_time))