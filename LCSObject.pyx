from utils import is_number

class LCSObject(object):
    def __init__(self, seq, lcs_object_id):
        self.seq = seq
        self.id = lcs_object_id
        self.num_matches = 1
        self.average_match_length = len(seq)

    def get_lcs_length(self, inp_seq):
        count = 0

        for token in self.seq:
            if token == "*":
                continue
            for i, inp_token in enumerate(inp_seq):
                if token == inp_token:
                    inp_seq = inp_seq[i+1:]
                    count += 1
                    break

        return count

    def merge(self, lcs_object):
        self.average_match_length = (self.num_matches * self.average_match_length + \
                                     lcs_object.num_matches * lcs_object.average_match_length) / \
                                     (self.num_matches + lcs_object.num_matches)
        self.num_matches += lcs_object.num_matches

    def update_statistics(self, inp_seq):
        self.average_match_length = (self.num_matches * self.average_match_length + len(inp_seq)) / (self.num_matches + 1)
        self.num_matches += 1

    def insert_seq(self, inp_seq):

        self.update_statistics(inp_seq)

        new_seq = []

        for token in self.seq:
            if token == "*":
                if len(new_seq) == 0 or new_seq[-1] != "*":
                    new_seq.append("*")
                continue

            for i, inp_token in enumerate(inp_seq):
                if token == inp_token:
                    if is_number(token):
                        if len(new_seq) == 0 or new_seq[-1] != "*":
                            new_seq.append("*")
                        break
                    new_seq.append(token)
                    inp_seq = inp_seq[i+1:]
                    break
                elif len(new_seq) != 0 and new_seq[-1] != "*":
                    new_seq.append("*")
                
                self.seq = new_seq
    
    def get_length(self):
        return len(self.seq)
    
    def __str__(self):
        return " ".join(self.seq)
