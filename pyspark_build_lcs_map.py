import argparse
import boto3
import pickle
import time

from pyspark.context import SparkContext
from pyspark.conf import SparkConf

from LCSMap import LCSMap
from nodeinfo import NodeInfo

def process_s3_files(sc, bucket, input_path,
                     output_path, num_tasks, 
                     write_output, file_limit):
    start_time = time.time()

    creds = get_s3_credentials()

    get_bucket = lambda: boto3.session\
                              .Session(**creds)\
                              .resource('s3')\
                              .Bucket(bucket)

    filenames = [x.key for x in get_bucket().objects.filter(Prefix=input_path)]

    filenames = filenames[:file_limit]

    print("Processing input files")  # Returns [lcsmap, s3_in_path]
    lcsmap = sc.parallelize(filenames, num_tasks)\
                   .filter(lambda x: x.endswith(".err"))\
                   .map(lambda x: (x, get_bucket().Object(x).get()['Body']._raw_stream))\
                   .map(lambda x: (LCSMap(write_output, pattern_limit), x))\
                   .map(lambda x: x[0].process_file(x[1][0], buffer=x[1][1], remove_duplicates=False))\
                   .treeReduce(lambda x, y: x.merge(y))

    print("Number of patterns: {}".format(len(lcsmap.lcs_objects)))

    print("Storing pickle")
    lcsmap_pickle = pickle.dumps(lcsmap)
    get_bucket().put_object(Key="{}/lcsmap.p".format(output_path), Body=lcsmap_pickle)
    
    if write_output:
        print("Writing output files")
        sc.parallelize(filenames, num_tasks)\
          .map(lambda x: (x, "{}/{}".format(output_path, x.split("/")[-1])))\
          .map(lambda x: (x[1], lcsmap.convert_file(x[0], write_to_buffer=True)))\
          .foreach(lambda x: get_bucket().put_object(Key=x[0], Body=x[1]))
        
    end_time = time.time()
    print("Process took: {} secs".format(end_time - start_time))


def get_s3_credentials():
    creds = {}
    creds["aws_access_key_id"] = NodeInfo.get_info("s3_access_key_id")
    creds["aws_secret_access_key"] = NodeInfo.get_info("s3_secret_access_key")

    return creds

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--bucket', type=str, default="dev.canopydata.com",
                        help='S3 bucket.')
    parser.add_argument('--input-path', type=str, default="ameya/sample_logs/",
                        help='S3 path to read files from.')
    parser.add_argument('--output-path', type=str, default=None,
                        help='S3 path to store output files.')       
    parser.add_argument('--num-tasks', type=int, default=2,
                        help='Number of spark tasks to start.')
    parser.add_argument('--write-output', type=bool, default=False,
                        help='If to write the converted files back to S3.')
    parser.add_argument('--pattern-limit', type=int, default=8000,
                        help='Maximum number of patterns after which we stop processing a file.')            
    parser.add_argument('--file-limit', type=int, default=100,
                        help='Maximum number of files to process.')            

    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()

    timestamp = int(time.time())
    output_path = "{}_output".format(args.input_path.rstrip("/")) \
                    if args.input_path is None else args.input_path
    output_path = "{}/{}".format(output_path, timestamp)

    sc = SparkContext(conf=SparkConf().setAppName("spell"))
    
    process_s3_files(sc, args.bucket, args.input_path, 
                     output_path, args.num_tasks, 
                     args.write_output, args.file_limit)