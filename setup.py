from setuptools import setup
from Cython.Build import cythonize


setup(
    name='spell-log-analyzer',
    version='0.0.1',
    description='Spell log analyzer.',
    ext_modules = cythonize("*.pyx")
)