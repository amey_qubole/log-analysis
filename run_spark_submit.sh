set -x

/usr/lib/spark/bin/spark-submit \
--py-files dist/spell_log_analyzer-0.0.1-py3.5-linux-x86_64.egg,\
nodeinfo.py \
--max-executors 9 \
--executor-memory 50G \
--executor-cores 8 \
--driver-memory 50G \
--conf spark.driver.maxResultSize=30G \
--conf spark.executorEnv.PYTHONHASHSEED=321 \
--conf spark.executorEnv.PYTHON_EGG_CACHE="./.python-eggs/" \
--conf spark.executorEnv.PYTHON_EGG_DIR="./.python-eggs/" \
--conf spark.driverEnv.PYTHON_EGG_CACHE="./.python-eggs/" \
--conf spark.driverEnv.PYTHON_EGG_DIR="./.python-eggs/" \
pyspark_build_lcs_map.py \
--input-path "ameya/log_analysis" \
--num-tasks 1191